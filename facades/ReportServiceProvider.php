<?php namespace Entopancore\Report\Facades;

use Entopancore\Report\Classes\Report;
use October\Rain\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('entopancore.report', function () {
            return new Report();
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
}