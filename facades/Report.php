<?php namespace Entopancore\Report\Facades;

use October\Rain\Support\Facade;

class Report extends Facade
{
    /**
     * Get the registered name of the component.
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'entopancore.report';
    }
}
