<?php namespace Entopancore\Report\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Dump extends Migration
{
    public function up()
    {
        $file=\File::get(plugins_path() . "/entopancore/report/updates/versions/v1.sql");
        \DB::unprepared($file);
    }

    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('entopancore_report_preferences');
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}