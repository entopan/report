<?php namespace Entopancore\Report\Traits;

trait WidgetTrait
{

    public function init()
    {
        if (!method_exists(get_called_class(), 'getData')) {
            throw new \Exception(sprintf(
                'You must define a getData method in %s to use the Widget trait.', get_called_class()
            ));
        }
        if (!method_exists(get_called_class(), 'prepareVars')) {
            throw new \Exception(sprintf(
                'You must define a prepareVars method in %s to use the Widget trait.', get_called_class()
            ));
        }

        if (!method_exists(get_called_class(), 'render')) {
            throw new \Exception(sprintf(
                'You must define a render method in %s to use the Widget trait.', get_called_class()
            ));
        }

        if (!method_exists(get_called_class(), 'getConfiguration')) {
            throw new \Exception(sprintf(
                'You must define a getConfiguration method in %s to use the Widget trait.', get_called_class()
            ));
        }
        $this->vars["title"] = $this->title;
        $this->vars["configuration"] = json_encode(self::getConfiguration());
    }

    private function getVars($key)
    {

        $preferences = $this->getWidgetPreferences();
        return $preferences[$key];
    }

    private function setVars($key, $value)
    {
        $backendId = \BackendAuth::getUser()->id;
        $preferences = $this->getWidgetPreferencesDatabase();
        $preferences[$key] = $value;
        $preferences = json_encode($preferences);
        \Db::table("entopancore_report_preferences")->where("user_id", "=", \BackendAuth::getUser()->id)->where("key", $this->key)->update(["preferences" => $preferences]);
        \Cache::put('entopancore_report_preferences_' . $backendId . '_' . $this->key, $preferences, 60);

    }

    private function getWidgetPreferencesDatabase()
    {
        $backendId = \BackendAuth::getUser()->id;
        $data = \Db::table("entopancore_report_preferences")->where("user_id", "=", $backendId)->where("key", $this->key)->first();
        return json_decode($data->preferences, true);
    }

    private function getWidgetPreferences()
    {
        if($backend = \BackendAuth::getUser())
        {
           $backendId= $backend->id;
            $preferences = json_encode($this->preferences);

            $data = \Cache::remember('entopancore_report_preferences_' . $backendId . '_' . $this->key, 60, function () use ($backendId, $preferences) {
                if ($query = \Db::table("entopancore_report_preferences")->where("user_id", "=", $backendId)->where("key", $this->key)->first()) {
                    return $query->preferences;
                } else {
                    \Db::table("entopancore_report_preferences")->insert(["key" => $this->key, "preferences" => $preferences, "user_id" => $backendId]);
                    return \Db::table("entopancore_report_preferences")->where("user_id", "=", $backendId)->where("key", $this->key)->first()->preferences;
                }

           });

            $data = json_decode($data, true);
            return $data;
        }

    }

    public function setLabelCount($object)
    {
        $dates = json_decode(json_encode($object), true);
        foreach ($dates as $value) {
            $result[$value["label"]] = $value["count"];
        }
        return $result;
    }

}