<?php

return
    [
        'palettes' => [
            //base neutral
            '0' => [
                'background' => "transparent",
                "line" => "#000000",
                "icon" => "#000000",
                "text" => "#000000",
                "title" => "#000000",
                "item" => "#000000",
                "description" => "#000000"
            ],
            //base rosso
            '1' => [
                'background' => "#d24726",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //base arancio
            '2' => [
                'background' => "#f28c33",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //base giallo
            '3' => [
                'background' => "#f5d63d",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],

            //base verde
            '4' => [
                'background' => "#79c267",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //base blu
            '5' => [
                'background' => "#459ba8",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //base celeste
            '6' => [
                'background' => "#78c5d6",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //base viola
            '7' => [
                'background' => "#bf62a6",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //base marrone
            '8' => [
                'background' => "#87603e",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //base verde iguana
            '9' => [
                'background' => "#7CAE7A",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //vinaccia scuro
            '10' => [
                'background' => "#820040",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //rosa pallido
            '11' => [
                'background' => "#FF82C2",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //senso
            '12' => [
                'background' => "#671D2F",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],
            //blu scuro
            '13' => [
                'background' => "#34495e",
                "line" => "#FFFFFF",
                "icon" => "#FFFFFF",
                "text" => "#FFFFFF",
                "title" => "#FFFFFF",
                "item" => "#FFFFFF",
                "description" => "#FFFFFF"
            ],




        ],

    ];