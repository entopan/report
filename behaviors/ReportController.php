<?php namespace Entopancore\Report\Behaviors;

use Db;
use Str;
use Lang;
use Flash;
use Event;
use Input;
use Redirect;
use Backend;
use Backend\Classes\ControllerBehavior;
use ApplicationException;


class ReportController extends ControllerBehavior
{

    protected $requiredProperties = ['reportConfig', "reportWidget"];


    public $controller;

    public $config;

    public function __construct($controller)
    {
        parent::__construct($controller);

        $this->controller = $controller;
        $this->config = $this->makeConfig($controller->reportConfig);

        foreach ($this->config->{$controller->reportWidget} as $key => $widget) {
            $this->bindWidget($key);
        }

        foreach ($this->config->css as $css) {
            $this->getCss($css);
        }
        foreach ($this->config->js as $js) {
            $this->getJs($js);
        }
    }

    public function bindWidget($key)
    {
        $class = $this->config->{$this->controller->reportWidget}[$key];
        $widget = new $class($this->controller);
        $widget->bindToController();
    }

    public function renderWidget($key)
    {
        $class = $this->config->{$this->controller->reportWidget}[$key];
        $widget = new $class($this);
        return $widget->render();
    }

    public function getCss($type)
    {
        switch ($type) {
            case "scoreboard":
                $this->controller->addCss("/plugins/entopancore/report/assets/scoreboard.css");
                break;
            case "dashboard":
                $this->controller->addCss("/plugins/entopancore/report/assets/dashboard.css");
                break;
            default:
                break;
        }
    }

    public function getJs($type)
    {
        switch ($type) {
            case "chart":
                $this->controller->addJs("/plugins/entopancore/report/assets/chart/chartjs/bundle.js");
                $this->controller->addJs("/plugins/entopancore/report/assets/chart/utils.js");
                break;
        }
    }

}