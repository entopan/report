<?php namespace Entopancore\Report;

use Illuminate\Foundation\AliasLoader;
use System\Classes\PluginBase;
use Validator;

/**
 * Utility Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */

    public function register()
    {
        \App::register("Entopancore\Report\Facades\ReportServiceProvider");
        AliasLoader::getInstance()->alias("ElReport", "Entopancore\Report\Facades\Report");
    }

    public function pluginDetails()
    {
        return [
            'name' => 'Report',
            'description' => 'Report',
            'author' => 'Entopancore',
            'icon' => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */


}
