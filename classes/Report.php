<?php namespace Entopancore\Report\Classes;

use System\Traits\ViewMaker;

class Report
{
    use ViewMaker;

    public function getCss($controller, $type)
    {
        switch ($type) {
            case "scoreboard":
                $controller->addCss("/plugins/entopancore/report/assets/scoreboard.css");
                break;
            case "dashboard":
                $controller->addCss("/plugins/entopancore/report/assets/dashboard.css");
                break;
            default:
                break;
        }
    }

    public function getChartJs($controller)
    {
        $controller->addJs("/plugins/entopancore/report/assets/chart/chartjs/bundle.js");
        $controller->addJs("/plugins/entopancore/report/assets/chart/utils.js");
    }


    protected function truncateTime($date)
    {
        $createDate = new \DateTime($date);
        return $createDate->format('d-m-Y');
    }

    public function makeScoreboard($type, $data)
    {
        $data = $this->setData($data);
        return $this->makePartial('~/plugins/entopancore/report/partials/scoreboard/_' . $type . '.htm', ["data" => $data]);
    }

    public function makeModule($type, $data)
    {
        $data = $this->setData($data);
        return $this->makePartial('~/plugins/entopancore/report/partials/modules/_' . $type . '.htm', ["data" => $data]);
    }

    public function makeChart($type, $data)
    {
        switch ($type) {
            case "pie":
                $array = $this->setChartData($data["data"], $data["title"]);
                break;
            case "bubble":
                $array = $this->setChartMultiData($data);
                break;
            case "multipie":
                $array = $this->setChartMultiData($data);
                break;
            case "multibar":
                $array = $this->setChartMultiData($data);
                break;
            case "doughnut":
                $array = $this->setChartData($data["data"], $data["title"]);
                break;
            case "polar-area":
                $array = $this->setChartData($data["data"], $data["title"]);
                break;
            case "line":
                $array = $this->setChartMultiData($data);
                break;
            case "bar":
                $array = $this->setChartData($data["data"], $data["title"]);
                break;

        }

        return $this->makePartial(
            '~/plugins/entopancore/report/partials/chart/_' . $type . '.htm',
            [
                "data" => $array,
                "title" => ($data["title"]) ?? null,
                "titlexAxes" => ($data["titlexAxes"]) ?? null,
                "titleyAxes" => ($data["titleyAxes"]) ?? null,
                "random" => rand(10000, 20000000),
                "legend_position" => ($data["legend_position"]) ?? 'bottom',
                "legend_display" => ($data["legend_display"]) ?? false,
            ]);
    }

    public function makeWidget($type, $data)
    {
        $data = $this->setData($data);
        return $this->makePartial('~/plugins/entopancore/report/partials/modules/_' . $type . '.htm', ["data" => $data]);
    }

    private function setData($data)
    {

        $default = [
            "palette" => 0,
            "features" => [],
            "layout" => 6,
            "title" => '',
            "titlebar" => '',
            "titletable" => '',
            "description" => '',
            "items" => 0,
            "itemsbar" => 0,
            "itemstable" => 0,
            "icon" => '',
            "class" => '',
            "path" => '#',

        ];
        $data = array_merge($default, $data);
        $style = $this->makeStyle($data["palette"]);
        $data = array_merge($data, $style);
        if ($data["features"]) {
            $data["layout"] = 5;
        }
        if (count($data["features"]) == 0) {
            unset($data["features"]);
        }
        return $data;

    }

    private function makeStyle($palette)
    {
        return [
            "background" => config("entopancore.report::palettes.$palette.background"),
            "colorline" => config("entopancore.report::palettes.$palette.line"),
            "coloricon" => config("entopancore.report::palettes.$palette.icon"),
            "colortext" => config("entopancore.report::palettes.$palette.text"),
            "colortitle" => config("entopancore.report::palettes.$palette.title"),
            "coloritem" => config("entopancore.report::palettes.$palette.item"),
            "colordescription" => config("entopancore.report::palettes.$palette.colordescription"),
        ];
    }


    private function setChartData($array, $title)
    {
        $label = array();
        $dataset = array();

        foreach ($array as $k => $a) {
            array_push($label, $k);
            array_push($dataset, $a);
        }
        $array = array(
            "labels" => $label,
            "datasets" =>
                [
                    $this->composeArray($dataset, $title),
                ],
        );
        return json_encode($array);
    }


    private function setChartMultiData($data)
    {
        $array = $data["data"];
        $labelChart = $data["labelChart"];

        if (!is_array($array)) {
            return null;
        }
        if (!is_array((array_values($array)[0]))) {
            $array = [$array];
        }

        if (isset($labelChart)) {
            if (!is_array($labelChart)) {
                $labelChart = [$labelChart];
            }
        }

        $result = array("labels" => array(), "datasets" => array());
        $label = array();
        $datasets = array();


        foreach ($array as $key => $value) {
            foreach ($value as $k => $v) {
                array_push($label, $k);
                $datasets[$key][$k] = $v;
            }
        }
        $label = array_unique($label);
        $label = array_values($label);
        $x = array();
        foreach ($datasets as $key => $dataset) {
            foreach ($label as $k => $l) {
                if (isset($dataset[$l])) {
                    array_push($x, $dataset[$l]);
                } else {
                    array_push($x, 0);
                }
            }
            array_push($result["datasets"], $this->composeMultiArray($x, $labelChart[$key]));
            $x = array();
        }
        $result["labels"] = array_unique($label);
        return json_encode($result);
    }

    protected function changeScale($value, $active)
    {
        if ($active) {
            $value = $value * 20;
        }
        return $value;
    }

    private function getRandomColor($length)
    {
        $colors = ["#335C67", "#FFF3B0", "#E09F3E", "#9E2A2B", "#540B0E", "#E1CA96", "#ACA885", "#918B76", "#626C66", "#434A42"];

        if ($length == "random") {
            shuffle($colors);
            return $colors[0];
        }
        return array_splice($colors, 0, $length);
    }

    private function composeArray($data, $title)
    {
        $length = count($data);
        $colors = $this->getRandomColor($length);
        return ["label" => $title, "data" => $data, "backgroundColor" => $colors, "hoverBackgroundColor" => $colors];

    }

    private function composeMultiArray($data, $label)
    {
        $colors = $this->getRandomColor("random");
        return ["label" => $label, "data" => $data, "borderColor" => $colors, "backgroundColor" => $colors, "hoverBackgroundColor" => $colors, "data" => $data];

    }

}
